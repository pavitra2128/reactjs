import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';

const Status = () => {
    const navigate = useNavigate();
    const [dateData, setDateData] = useState(null);
    const [status, setStatus] = useState(false);
    const [status2, setStatus2] = useState(false);

    useEffect(() => {
        const fetchData = async () => {
            try {
                const userId = localStorage.getItem('userId');
                const response = await axios.get(`http://localhost:3001/date-details/${userId}`);
                setDateData(response.data);
                
                const statusResponse = await axios.get(`http://localhost:3001/status/${userId}`);
                setStatus(statusResponse.data.status === 'approved');
                setStatus2(statusResponse.data.status2 === 'approved');
            } catch (error) {
                console.error('Error fetching data:', error);
            }
        };
        fetchData();
    }, []);

    const formatDate = (dateString) => {
        if (!dateString) return "";
        const date = new Date(dateString);
        const day = date.toLocaleString('en-us', { weekday: 'long' });
        return `${date.toDateString()}, ${day}`;
    };

    const handleSubmit = () => {
        navigate('/student');
    };

    const handleCancel = async () => {
        const userId = localStorage.getItem('userId');
        const statusData = {
            status: 'cancelled'
        };
        try {
            await axios.post(`http://localhost:3001/update-status/${userId}`, statusData);
            localStorage.setItem('status', 'cancelled');
            navigate('/');
        } catch (error) {
            console.error('Error cancelling request:', error);
        }
    };

    const Instructions = () => {
        return (
            <div className="bg-light p-4 rounded mt-4">
                <h2 className="text-primary">Instructions</h2>
                <p>Checkout is permitted starting one hour before the requested time.</p>
                <p><strong>Outing types:</strong></p>
                <ul>
                    <li><strong>Out-city:</strong><br />More than 1 day when traveling out of the city.</li>
                    <li><strong>Local:</strong><br />Single day outing (Within city 9AM - 6PM).</li>
                    <li><strong>Temporary Day Scholar:</strong><br />Temporarily become a day scholar for some days.</li>
                    <li><strong>Official:</strong><br />Campus admin will declare it and you can apply for it when available.</li>
                </ul>
            </div>
        );
    };

    return (
        <div className='container mt-5'>
            <button type='button' onClick={handleSubmit} className='btn btn-primary btn-lg mb-4'>
                <i className="fas fa-plus-circle mr-2"></i>Create Request
            </button>
            <button type='button' onClick={handleCancel} className='btn btn-danger btn-lg mb-4 ml-2'>
                Cancel Request
            </button>
            <div className='row'>
                <div className='col-12 col-md-6'>
                    <div className='card mb-4'>
                        <div className='card-body'>
                            <h5 className='card-title'>From Date</h5>
                            <p className='card-text'>{dateData && formatDate(dateData.from_date)}</p>
                        </div>
                    </div>
                </div>
                <div className='col-12 col-md-6'>
                    <div className='card mb-4'>
                        <div className='card-body'>
                            <h5 className='card-title'>To Date</h5>
                            <p className='card-text'>{dateData && formatDate(dateData.to_date)}</p>
                        </div>
                    </div>
                </div>
            </div>
            <div className='form-check mb-4'>
                <input type='checkbox' id='submitted' className='form-check-input' checked={dateData !== null} readOnly />
                <label htmlFor='submitted' className='form-check-label'>Submitted</label>
                <hr className='dotted-line' />
            </div>
            <div className='form-check mb-4'>
                <input type='checkbox' id='status' className='form-check-input' checked={status=='approved'} readOnly />
                <label htmlFor='status' className='form-check-label'>Approved By HOD </label>
                <FontAwesomeIcon icon={faCheck} className={status ? 'text-primary' : 'd-none'} />
            </div>
            <div className='form-check mb-4'>
                <input type='checkbox' id='status2' className='form-check-input' checked={status2=='approved'} readOnly />
                <label htmlFor='status2' className='form-check-label'>Issued By Warden</label>
                <FontAwesomeIcon icon={faCheck} className={status2 ? 'text-primary' : 'd-none'} />
            </div>
            <Instructions />
        </div>
    );
};

export default Status;
