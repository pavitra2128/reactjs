import React, { useEffect, useState } from 'react';
import Chart from 'chart.js/auto';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHourglass, faCheckCircle, faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

const Dashboard = () => {
  const [pendingCount, setPendingCount] = useState(0);
  const [approvedCount, setApprovedCount] = useState(0);

  const renderBarGraph = () => {
    const ctx = document.getElementById('barGraph');
    if (ctx) {
      Chart.getChart(ctx)?.destroy();
      new Chart(ctx, {
        type: 'bar',
        data: {
          labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August'],
          datasets: [{
            label: 'Total Outings',
            data: [10, 20, 15, 10, 22, 20, 6, 8],
            backgroundColor: 'rgba(54, 162, 235, 0.5)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
          }]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          }
        }
      });
    } else {
      console.error('Element with ID "barGraph" not found.');
    }
  };

  useEffect(() => {
    fetchCounts();
    renderBarGraph();
  }, []);

  const fetchCounts = () => {
    fetch('http://localhost:3001/admin/count/pending')
      .then(response => response.json())
      .then(data => setPendingCount(data.count))
      .catch(error => console.error('Error fetching pending requests count:', error.message));

    fetch('http://localhost:3001/admin/approved')
      .then(response => response.json())
      .then(data => setApprovedCount(data.length))
      .catch(error => console.error('Error fetching approved requests count:', error.message));
  };

  return (
    <main className="container-fluid">
      <div className="row">
        <div className="col-lg-4 mb-4">
          <div className="card text-white bg-info">
            <div className="card-body">
              <h3 className="card-title"><FontAwesomeIcon icon={faHourglass} /> Pending Requests</h3>
              <h1 className="card-text display-4">{pendingCount}</h1>
            </div>
          </div>
        </div>
        <div className="col-lg-4 mb-4">
          <div className="card text-white bg-success">
            <div className="card-body">
              <h3 className="card-title"><FontAwesomeIcon icon={faCheckCircle} /> Approved Requests</h3>
              <h1 className="card-text display-4">{approvedCount}</h1>
            </div>
          </div>
        </div>
        <div className="col-lg-4 mb-4">
          <div className="card text-white bg-danger">
            <div className="card-body">
              <h3 className="card-title"><FontAwesomeIcon icon={faExclamationCircle} /> Delayed Requests</h3>
              <h1 className="card-text display-4">15</h1>
            </div>
          </div>
        </div>
      </div>
      <div className="row">
        <div className="col-lg-12">
          <div className="card">
            <div className="card-body">
              <h5 className="card-title">Bar Graph</h5>
              <canvas id="barGraph" width="500" height="125"></canvas>
            </div>
          </div>
        </div>
      </div>
    </main>
  );
};

export default Dashboard;
