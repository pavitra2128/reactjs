// OutingRequest.jsx
import React from 'react';
import { Link } from 'react-router-dom';

const OutingRequest = () => {
  return (
    <div className="container-fluid p-0">
      <div className="bg-primary text-white py-4 text-center">
        <Link to="/profile/:id" style={profileButtonStyle}>Profile</Link>
        <h1>Outing Requests</h1>
        <p className="lead">All your requests at one place</p>
      </div>
      <div className="bg-light p-4 rounded mt-6">
        <div className="outing-request">
          <Link to="/request" className="btn btn-primary btn">Create Outing Request ➤</Link>
          <p className="mt-2">No active requests!</p>
        </div>
      </div>
      <Instructions />
    </div>
  );
};

function Instructions() {
  return (
    <div className="bg-light p-4 rounded mt-4">
      <h2 className="text-primary">Instructions</h2>
      <p>Checkout is permitted starting one hour before the requested time.</p>
      <p><strong>Outing types:</strong></p>
      <ul>
        <li><strong>Out-city:</strong><br />More than 1 day when travelling out of the city.</li>
        <li><strong>Local:</strong><br />Single day outing (Within city 9AM - 6PM).</li>
        <li><strong>Temporary Day Scholar:</strong><br />Temporarily become a day scholar for some days.</li>
        <li><strong>Official:</strong><br />Campus admin will declare it and you can apply for it when available.</li>
      </ul>
    </div>
  );
}

const profileButtonStyle = {
  position: 'absolute',
  top: '10px',
  right: '10px',
  textDecoration: 'none',
  padding: '10px 20px',
  backgroundColor: '#007bff',
  color: '#fff',
  borderRadius: '5px'
};

export default OutingRequest;
