import React from 'react';
import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import OutingRequest from './outing';
import Admin from './Admin';
import Dashboard from './Dashboard';
import Status from './status';
import Pending from './pending'; 
import Approved from './Approved';
import LoginPage from './login';
import RadioButtonGroup from './request';
import Rejected from './rejected';
import Warden from './warden';
import Studentprofile from './profile';
import InOuting from './inouting';
import ApprovedWarden from './ApprovedWarden';
import RejectedWarden from './RejectedWarden';
import WardenRequest from './WardenRequests';
const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/" element={<LoginPage />} />
        <Route path="/student" element={<OutingRequest />} />
        <Route path="/admin/*" element={<AdminLayout />} />
        <Route path="/status" element={<Status />} />
        <Route path="/request" element={<RadioButtonGroup />} />
        <Route path="/profile/:id" element={<Studentprofile />} />
        <Route path="/warden/*" element={<WardenLayout />} />
      </Routes>
    </Router>
  );
};

const AdminLayout = () => {
  return (
    <Admin>
      <Routes>
        <Route path="/" element={<Dashboard />} />
        <Route path="/approved" element={<Approved />} />
        <Route path="/pending" element={<Pending />} />
        <Route path="/rejected" element={<Rejected />} />
        <Route path="/inouting" element={<InOuting />} />
      </Routes>
    </Admin>
  );
};

const WardenLayout = () => {
  return (
    <Warden>
      <Routes>
        <Route path="/" element={<WardenRequest />} />
        <Route path="/approved" element={<ApprovedWarden />} />
        <Route path="/rejected" element={<RejectedWarden />} />
      </Routes>
    </Warden>
  );
};

export default App;