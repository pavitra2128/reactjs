// Backend (server.js)
const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(express.json());

const connection = mysql.createPool({
  host: 'localhost',
  user: 'root',
  password: '', 
  database: 'users'
});

connection.getConnection(error => {
  if (error) {
    console.error('Error connecting to MySQL database:', error);
    return;
  }
  console.log('Connected to MySQL database');
});

app.get('/status/:id', (req, res) => {
  const { id } = req.params;

  connection.query(
    'SELECT status, status2 FROM outing_requests WHERE id = ?',
    [id],
    (error, results) => {
      if (error) {
        console.error('Error fetching status data:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      if (results.length === 0) {
        res.status(404).json({ error: 'Status data not found' });
        return;
      }
      res.json(results[0]); 
    }
  );
});
app.post('/update-details/:id', (req, res) => {
  const { id } = req.params;
  const { checkInTime, checkOutTime, startDate, endDate, reason, status } = req.body; 
  connection.query(
    'UPDATE outing_requests SET in_time = ?, out_time = ?, from_date = ?, to_date = ?, reason = ?, status = ? WHERE id = ?',
    [checkInTime, checkOutTime, startDate, endDate, reason, status, id], 
    (error, results) => {
      if (error) {
        console.error('Error updating user details:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      res.json({ message: 'User details updated successfully' });
    }
  );
});
app.post('/update-status/:id', (req, res) => {
  const { id } = req.params;
  const { status } = req.body;
  connection.query(
    'UPDATE outing_requests SET status = ? WHERE id = ?',
    [status, id],
    (error, results) => {
      if (error) {
        console.error('Error updating status:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      res.json({ message: 'Status updated successfully' });
    }
  );
});



app.post('/login', (req, res) => {
    const { email, password } = req.body;
    const sql = "SELECT id, role FROM outing_requests WHERE email = ? AND password = ?";
    
    connection.query(sql, [email, password], (err, data) => {
        if (err) {
            console.error('Error executing SQL query:', err);
            res.status(500).json({ success: false, message: 'Internal Server Error' });
            return;
        }
        if (data.length > 0) {
            const { id, role } = data[0]; 
            res.json({ success: true, id, role }); // Include role in the response
        } else {
            res.json({ success: false, message: 'Invalid credentials' });
        }
    });
});

app.get('/pending-requests', (req, res) => {
  connection.query('SELECT * FROM outing_requests WHERE status = "pending"', (error, results) => {
    if (error) {
      console.error('Error fetching pending requests:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(results);
  });
});

app.get('/date-details/:id', (req, res) => {
  const { id } = req.params;
connection.query('SELECT from_date, to_date FROM outing_requests WHERE id = ?',[id], (error, results) => {
    if (error) {
      console.error('Error fetching date details:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }

    if (results.length === 0) {
      res.status(404).json({ error: 'User not found' });
      return;
    }

    const { from_date, to_date } = results[0];
    res.json({ from_date, to_date });
  });
});
// Get all approved requests
app.get('/approved-requests', (req, res) => {
  connection.query('SELECT * FROM outing_requests WHERE status = "approved"', (error, results) => {
    if (error) {
      console.error('Error fetching approved requests:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(results);
  });
});

app.get('/approved-requestsWarden', (req, res) => {
  connection.query('SELECT * FROM outing_requests WHERE status = "approved" AND status2 IS NULL', (error, results) => {
    if (error) {
      console.error('Error fetching approved requests:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(results);
  });
});
app.get('/approved-requests2', (req, res) => {
    connection.query('SELECT * FROM outing_requests WHERE status2 = "approved"', (error, results) => {
      if (error) {
        console.error('Error fetching approved requests:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      res.json(results);
    });
  });

// Get all rejected requests
app.get('/rejected-requests', (req, res) => {
  connection.query('SELECT * FROM outing_requests WHERE status = "rejected"', (error, results) => {
    if (error) {
      console.error('Error fetching rejected requests:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(results);
  });
});
app.get('/rejected-requests2', (req, res) => {
  connection.query('SELECT * FROM outing_requests WHERE status2 = "rejected"', (error, results) => {
    if (error) {
      console.error('Error fetching rejected requests:', error);
      res.status(500).json({ error: 'Internal Server Error' });
      return;
    }
    res.json(results);
  });
});
app.put('/reject-request/:id', (req, res) => {
  const { id } = req.params;
  const { reason } = req.body;
  if (!reason) {
    return res.status(400).json({ error: 'Rejected reason is required' });
  }

  connection.query(
    'UPDATE outing_requests SET status = "rejected" ,rejected_reason=? WHERE id = ?',
    [reason, id],
    (error, results) => {
      if (error) {
        console.error('Error rejecting request:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      res.json({ message: 'Request rejected successfully' });
    }
  );
});
app.put('/reject-request2/:id', (req, res) => {
  const { id } = req.params;
  const { reason } = req.body;
  if (!reason) {
    return res.status(400).json({ error: 'Rejected reason is required' });
  }
    connection.query(
      'UPDATE outing_requests SET status2 = "rejected",rejected_reason2=? WHERE id = ?',
      [reason,id],
      (error, results) => {
        if (error) {
          console.error('Error rejecting request:', error);
          res.status(500).json({ error: 'Internal Server Error' });
          return;
        }
        res.json({ message: 'Request rejected successfully' });
      }
    );
  });


app.put('/approve-request/:id', (req, res) => {
  const { id } = req.params;

  connection.query(
    'UPDATE outing_requests SET status = "approved" WHERE id = ?',
    [id],
    (error, results) => {
      if (error) {
        console.error('Error approving request:', error);
        res.status(500).json({ error: 'Internal Server Error' });
        return;
      }
      res.json({ message: 'Request approved successfully' });
    }
  );
});
app.put('/approve-request2/:id', (req, res) => {
    const { id } = req.params;
  
    connection.query(
      'UPDATE outing_requests SET status2 = "approved" WHERE id = ?',
      [id],
      (error, results) => {
        if (error) {
          console.error('Error approving request:', error);
          res.status(500).json({ error: 'Internal Server Error' });
          return;
        }
        res.json({ message: 'Request approved successfully' });
      }
    );
  });

app.put('/cancel-request/:id', (req, res) => {
    const { id } = req.params;
  
    connection.query(
      'UPDATE outing_requests SET status = "cancelled" WHERE id = ?',
      [id],
      (error, results) => {
        if (error) {
          console.error('Error cancelling request:', error);
          res.status(500).json({ error: 'Internal Server Error' });
          return;
        }
        res.json({ message: 'Request cancelled successfully' });
      }
    );
  });
  app.get('/status/:id', (req, res) => {
    const { id } = req.params;
    connection.query(
      'SELECT status, status2 FROM outing_requests WHERE user_id = ?',
      [id],
      (error, results) => {
        if (error) {
          console.error('Error fetching status data:', error);
          res.status(500).json({ error: 'Internal Server Error' });
          return;
        }
        if (results.length === 0) {
          res.status(404).json({ error: 'Status data not found' });
          return;
        }
        res.json(results[0]);
      }
    );
  });

const PORT = 3001;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});